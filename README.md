## Usage
    yarn install
    ./fleet create <userId>

## Local tests
    docker compose -f tests/docker-compose.test.yml up -d
    yarn install
    yarn test:unit
    yarn test:int

## Code quality tools
    prettier / eslint
        formatter and linter in order to homogenize code and have commit changes easiest to read

## CI
    I used circleci for continus integration.
    Necessary actions
        - connect circleci to my gitlab repository
        - configure .circleci/config.yml
        - add docker-compose file for CI in order to easly tests all types of tests
        (the second goal is to use same service dockerfile in CI and in production environment)

        