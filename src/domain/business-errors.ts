export const vehicle_already_registered =
  "this vehicle has already been registered into my fleet";

export const vehicle_already_parked_at_the_same_location =
  "this vehicle is already parked at the same location";

export const vehicle_does_not_exist = "this vehicle does not exist";

export const fleet_does_not_exist = "this fleet does not exist";
