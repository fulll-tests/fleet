import { Vehicle } from "../vehicle";

export interface IVehiclesRepository {
  getByPlateNumber(plateNumber: string): Promise<Vehicle>;

  save(vehicle: Vehicle): Promise<void>;
}
