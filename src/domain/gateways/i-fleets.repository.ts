import { Fleet } from "../fleet";

export interface IFleetsRepository {
  save(fleet: Fleet): Promise<void>;

  getById(id: string): Promise<Fleet>;
}
