import { Location } from "./location";
import { IUuidGenerator } from "./gateways/i-uuid.generator";
import { BusinessError } from "./business-error";
import { vehicle_already_parked_at_the_same_location } from "./business-errors";

export class Vehicle {
  constructor(
    readonly id: string,
    readonly plateNumber: string,
    public location: Location
  ) {}

  static create(
    uuidGenerator: IUuidGenerator,
    vehiclePlateNumber: string
  ): Vehicle {
    return new Vehicle(
      uuidGenerator.generate(),
      vehiclePlateNumber,
      new Location(null, null, null)
    );
  }

  isSame(vehicle: Vehicle): boolean {
    return this.plateNumber === vehicle.plateNumber;
  }

  park(lat: number, lng: number, alt: number): void {
    const location = new Location(lat, lng, alt);

    if (this.location.isSame(location))
      throw new BusinessError(vehicle_already_parked_at_the_same_location);

    this.location = location;
  }
}
