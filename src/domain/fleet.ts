import { Vehicle } from "./vehicle";
import { BusinessError } from "./business-error";
import { vehicle_already_registered } from "./business-errors";
import { IUuidGenerator } from "./gateways/i-uuid.generator";
import { User } from "./user";

export class Fleet {
  constructor(
    readonly id: string,
    readonly user: User,
    readonly vehicles: Vehicle[]
  ) {}

  static create(uuidGenerator: IUuidGenerator, user: User): Fleet {
    return new Fleet(uuidGenerator.generate(), user, []);
  }

  register(vehicle: Vehicle): void {
    this.throwIfVehicleAlreadyRegistered(vehicle);
    this.vehicles.push(vehicle);
  }

  getVehicle(plateNumber: string): Vehicle {
    return this.vehicles.find((vehicle) => vehicle.plateNumber === plateNumber);
  }

  // PRIVATE METHODS

  private throwIfVehicleAlreadyRegistered(vehicleToRegistered: Vehicle): void {
    const alreadyRegistered = this.vehicles.some((vehicle) =>
      vehicle.isSame(vehicleToRegistered)
    );
    if (alreadyRegistered) throw new BusinessError(vehicle_already_registered);
  }
}
