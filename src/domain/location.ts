export class Location {
  constructor(
    readonly lat: number,
    readonly lng: number,
    readonly alt?: number
  ) {}

  isSame(location: Location): boolean {
    return (
      this.lat === location.lat &&
      this.lng === location.lng &&
      this.alt === location.alt
    );
  }
}
