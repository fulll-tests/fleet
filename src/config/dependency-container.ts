import { CreateFleetHandler } from "../app/create-fleet/create-fleet.handler";
import { ParkVehicleHandler } from "../app/park-vehicle/park-vehicle.handler";
import { AppDataSource } from "../infra/secondary/postgres/data-source";
import { FleetsPgRepository } from "../infra/secondary/postgres/repositories/fleets-pg.repository";
import { FleetModel } from "../infra/secondary/postgres/models/fleet.model";
import { VehiclesPgRepository } from "../infra/secondary/postgres/repositories/vehicles-pg.repository";
import { VehicleModel } from "../infra/secondary/postgres/models/vehicle.model";
import { RealUuidGenerator } from "../infra/secondary/real-providers/real-uuid.generator";
import { RegisterVehicleHandler } from "../app/register-vehicle/register-vehicle.handler";

export class DependencyContainer {
  public createFleetHandler: CreateFleetHandler;
  public parkVehicleHandler: ParkVehicleHandler;
  public registerVehicleHandler: RegisterVehicleHandler;

  async init(): Promise<void> {
    // Infra
    const dataSource = await AppDataSource.initialize();
    const fleetsRepository = new FleetsPgRepository(
      dataSource.getRepository(FleetModel)
    );
    const vehiclesRepository = new VehiclesPgRepository(
      dataSource.getRepository(VehicleModel)
    );
    const uuidGenerator = new RealUuidGenerator();

    // Handlers
    this.createFleetHandler = new CreateFleetHandler(
      fleetsRepository,
      uuidGenerator
    );
    this.parkVehicleHandler = new ParkVehicleHandler(
      fleetsRepository,
      vehiclesRepository
    );
    this.registerVehicleHandler = new RegisterVehicleHandler(
      fleetsRepository,
      vehiclesRepository,
      uuidGenerator
    );
  }
}
