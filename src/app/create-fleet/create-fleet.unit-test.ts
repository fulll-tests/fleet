import { CreateFleetHandler } from "./create-fleet.handler";
import { CreateFleetCommand } from "./create-fleet.command";
import { FleetsInMemoryRepository } from "../../infra/secondary/in-memory/fleets-in-memory.repository";
import { FleetFixture } from "../../../tests/fixtures/fleet-fixture";
import { DeterministUuidGenerator } from "../../infra/secondary/deteminist-providers/determinist-uuid.generator";

let fleetsRepository: FleetsInMemoryRepository;
let uuidGenerator: DeterministUuidGenerator;
let handler: CreateFleetHandler;

const instantiateServices = (): void => {
  fleetsRepository = new FleetsInMemoryRepository();
  uuidGenerator = new DeterministUuidGenerator();
  handler = new CreateFleetHandler(fleetsRepository, uuidGenerator);
};

beforeEach(async () => {
  instantiateServices();
});

describe("handle", () => {
  it("should create a fleet", async () => {
    // Arrange

    // Act
    await handler.handle(new CreateFleetCommand("u1"));

    // Assert
    expect(fleetsRepository.saveSpy).toStrictEqual(
      FleetFixture.create({ id: "1" })
    );
  });

  it("should return the created fleet", async () => {
    // Arrange

    // Act
    const result = await handler.handle(new CreateFleetCommand("u1"));

    // Assert
    expect(result).toStrictEqual(FleetFixture.create({ id: "1" }));
  });
});
