import { FleetFixture } from "../../../tests/fixtures/fleet-fixture";
import { FleetsPgRepository } from "../../infra/secondary/postgres/repositories/fleets-pg.repository";
import { AppDataSource } from "../../infra/secondary/postgres/data-source";
import { FleetModel } from "../../infra/secondary/postgres/models/fleet.model";
import { CreateFleetCommand } from "./create-fleet.command";
import { CreateFleetHandler } from "./create-fleet.handler";
import { RealUuidGenerator } from "../../infra/secondary/real-providers/real-uuid.generator";

let fleetsRepository: FleetsPgRepository;
let uuidGenerator: RealUuidGenerator;
let handler: CreateFleetHandler;

const instantiateServices = async (): Promise<void> => {
  const dataSource = await AppDataSource.initialize();
  fleetsRepository = new FleetsPgRepository(
    dataSource.getRepository(FleetModel)
  );
  uuidGenerator = new RealUuidGenerator();
  handler = new CreateFleetHandler(fleetsRepository, uuidGenerator);
};

beforeAll(async () => {
  await instantiateServices();
});

const clearDatabase = async (): Promise<void> => {
  await fleetsRepository.deleteAll();
};

beforeEach(async () => {
  await clearDatabase();
});

describe("handle", () => {
  it("should create a fleet", async () => {
    // Arrange

    // Act
    await handler.handle(new CreateFleetCommand("u1"));

    // Assert
    const fleets = await fleetsRepository.getAll();
    expect(fleets[0]).toStrictEqual(
      FleetFixture.create({
        id: fleets[0].id,
      })
    );
  });

  afterAll(async () => {
    await clearDatabase();
  });
});
