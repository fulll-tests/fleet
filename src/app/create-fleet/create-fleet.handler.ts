import { IHandler } from "../_shared/i-handler";
import { CreateFleetCommand } from "./create-fleet.command";
import { IFleetsRepository } from "../../domain/gateways/i-fleets.repository";
import { Fleet } from "../../domain/fleet";
import { IUuidGenerator } from "../../domain/gateways/i-uuid.generator";
import { User } from "../../domain/user";

export class CreateFleetHandler implements IHandler {
  constructor(
    readonly fleetsRepository: IFleetsRepository,
    readonly uuidGenerator: IUuidGenerator
  ) {}

  async handle(command: CreateFleetCommand): Promise<Fleet> {
    const user = new User(command.userId);

    const fleet = Fleet.create(this.uuidGenerator, user);

    await this.fleetsRepository.save(fleet);
    return fleet;
  }
}
