export interface IHandler {
  handle(command: unknown): unknown;
}
