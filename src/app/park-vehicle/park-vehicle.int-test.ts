import { FleetFixture } from "../../../tests/fixtures/fleet-fixture";
import { FleetsPgRepository } from "../../infra/secondary/postgres/repositories/fleets-pg.repository";
import { AppDataSource } from "../../infra/secondary/postgres/data-source";
import { FleetModel } from "../../infra/secondary/postgres/models/fleet.model";
import { VehicleFixture } from "../../../tests/fixtures/vehicle-fixture";
import { VehiclesPgRepository } from "../../infra/secondary/postgres/repositories/vehicles-pg.repository";
import { VehicleModel } from "../../infra/secondary/postgres/models/vehicle.model";
import { Location } from "../../domain/location";
import { ParkVehicleCommand } from "./park-vehicle.command";
import { ParkVehicleHandler } from "./park-vehicle.handler";

let fleetsRepository: FleetsPgRepository;
let vehiclesRepository: VehiclesPgRepository;
let handler: ParkVehicleHandler;

const instantiateServices = async (): Promise<void> => {
  const dataSource = await AppDataSource.initialize();
  vehiclesRepository = new VehiclesPgRepository(
    dataSource.getRepository(VehicleModel)
  );
  fleetsRepository = new FleetsPgRepository(
    dataSource.getRepository(FleetModel)
  );
  handler = new ParkVehicleHandler(fleetsRepository, vehiclesRepository);
};

beforeAll(async () => {
  await instantiateServices();
});

const clearDatabase = async (): Promise<void> => {
  await vehiclesRepository.deleteAll();
  await fleetsRepository.deleteAll();
};

beforeEach(async () => {
  await clearDatabase();
});

describe("handle", () => {
  it("should park a vehicle", async () => {
    // Arrange
    await fleetsRepository.save(
      FleetFixture.withVehicle(
        VehicleFixture.create({ location: new Location(null, null) })
      )
    );

    // Act
    await handler.handle(new ParkVehicleCommand("f1", "123-456-789", 1, 2, 3));

    // Assert
    const vehicles = await vehiclesRepository.getAll();
    expect(vehicles[0]).toStrictEqual(
      VehicleFixture.create({ location: new Location(1, 2, 3) })
    );
  });

  afterAll(async () => {
    await clearDatabase();
  });
});
