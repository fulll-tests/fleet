import { ParkVehicleHandler } from "./park-vehicle.handler";
import { ParkVehicleCommand } from "./park-vehicle.command";
import { FleetsInMemoryRepository } from "../../infra/secondary/in-memory/fleets-in-memory.repository";
import { FleetFixture } from "../../../tests/fixtures/fleet-fixture";
import { VehiclesInMemoryRepository } from "../../infra/secondary/in-memory/vehicles-in-memory.repository";
import { VehicleFixture } from "../../../tests/fixtures/vehicle-fixture";
import { Location } from "../../domain/location";
import { BusinessError } from "../../domain/business-error";
import {
  fleet_does_not_exist,
  vehicle_already_parked_at_the_same_location,
  vehicle_does_not_exist,
} from "../../domain/business-errors";
import { NotFoundError } from "../../domain/not-found-error";

let fleetsRepository: FleetsInMemoryRepository;
let vehiclesRepository: VehiclesInMemoryRepository;
let handler: ParkVehicleHandler;

const instantiateServices = (): void => {
  fleetsRepository = new FleetsInMemoryRepository();
  vehiclesRepository = new VehiclesInMemoryRepository();
  handler = new ParkVehicleHandler(fleetsRepository, vehiclesRepository);
};

beforeEach(async () => {
  instantiateServices();
});

describe("handle", () => {
  it("should park a vehicle", async () => {
    // Arrange
    fleetsRepository.data = [
      FleetFixture.withVehicle(
        VehicleFixture.create({ location: new Location(null, null) })
      ),
    ];

    // Act
    await handler.handle(new ParkVehicleCommand("f1", "123-456-789", 1, 2, 3));

    // Assert
    expect(vehiclesRepository.saveSpy).toStrictEqual(
      VehicleFixture.create({ location: new Location(1, 2, 3) })
    );
  });

  it("should throw when my vehicle already parked at the same location", async () => {
    // Arrange
    fleetsRepository.data = [
      FleetFixture.withVehicle(
        VehicleFixture.withLocation(new Location(1, 2, 3))
      ),
    ];

    try {
      // Act
      await handler.handle(
        new ParkVehicleCommand("f1", "123-456-789", 1, 2, 3)
      );
    } catch (e) {
      // Assert
      expect(e).toStrictEqual(
        new BusinessError(vehicle_already_parked_at_the_same_location)
      );
    }
    expect.assertions(1);
  });

  it("should return the parked vehicle", async () => {
    // Arrange
    fleetsRepository.data = [
      FleetFixture.withVehicle(
        VehicleFixture.create({ location: new Location(null, null) })
      ),
    ];

    // Act
    const result = await handler.handle(
      new ParkVehicleCommand("f1", "123-456-789", 1, 2, 3)
    );

    // Assert
    expect(result).toStrictEqual(
      VehicleFixture.create({ location: new Location(1, 2, 3) })
    );
  });

  it("should throw when fleet does not exist", async () => {
    // Arrange

    try {
      // Act
      await handler.handle(
        new ParkVehicleCommand("f1", null, null, null, null)
      );
    } catch (e) {
      // Assert
      expect(e).toStrictEqual(new NotFoundError(fleet_does_not_exist));
    }
    expect.assertions(1);
  });

  it("should throw when vehicle does not exist", async () => {
    // Arrange
    fleetsRepository.data = [FleetFixture.withoutVehicle()];

    try {
      // Act
      await handler.handle(
        new ParkVehicleCommand("f1", null, null, null, null)
      );
    } catch (e) {
      // Assert
      expect(e).toStrictEqual(new NotFoundError(vehicle_does_not_exist));
    }
    expect.assertions(1);
  });
});
