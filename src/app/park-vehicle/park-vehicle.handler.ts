import { IHandler } from "../_shared/i-handler";
import { Vehicle } from "../../domain/vehicle";
import { ParkVehicleCommand } from "./park-vehicle.command";
import { IVehiclesRepository } from "../../domain/gateways/i-vehicles.repository";
import { IFleetsRepository } from "../../domain/gateways/i-fleets.repository";
import { Fleet } from "../../domain/fleet";
import {
  fleet_does_not_exist,
  vehicle_does_not_exist,
} from "../../domain/business-errors";
import { NotFoundError } from "../../domain/not-found-error";

export class ParkVehicleHandler implements IHandler {
  constructor(
    readonly fleetsRepository: IFleetsRepository,
    readonly vehiclesRepository: IVehiclesRepository
  ) {}

  async handle(command: ParkVehicleCommand): Promise<Vehicle> {
    const fleet = await this.getFleet(command);
    const vehicle = this.getVehicle(fleet, command);

    vehicle.park(command.lat, command.lng, command.alt);

    await this.vehiclesRepository.save(vehicle);
    return vehicle;
  }

  // PRIVATE METHODS
  private async getFleet(command: ParkVehicleCommand): Promise<Fleet> {
    const fleet = await this.fleetsRepository.getById(command.fleetId);
    if (!fleet) throw new NotFoundError(fleet_does_not_exist);
    return fleet;
  }

  private getVehicle(fleet: Fleet, command: ParkVehicleCommand): Vehicle {
    const vehicle = fleet.getVehicle(command.vehiclePlateNumber);
    if (!vehicle) throw new NotFoundError(vehicle_does_not_exist);
    return vehicle;
  }
}
