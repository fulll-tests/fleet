export class ParkVehicleCommand {
  constructor(
    readonly fleetId: string,
    readonly vehiclePlateNumber: string,
    readonly lat: number,
    readonly lng: number,
    readonly alt?: number
  ) {}
}
