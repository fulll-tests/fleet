import { RegisterVehicleHandler } from "./register-vehicle.handler";
import { RegisterVehicleCommand } from "./register-vehicle.command";
import { FleetsInMemoryRepository } from "../../infra/secondary/in-memory/fleets-in-memory.repository";
import { FleetFixture } from "../../../tests/fixtures/fleet-fixture";
import { BusinessError } from "../../domain/business-error";
import {
  fleet_does_not_exist,
  vehicle_already_registered,
} from "../../domain/business-errors";
import { VehiclesInMemoryRepository } from "../../infra/secondary/in-memory/vehicles-in-memory.repository";
import { VehicleFixture } from "../../../tests/fixtures/vehicle-fixture";
import { NotFoundError } from "../../domain/not-found-error";
import { DeterministUuidGenerator } from "../../infra/secondary/deteminist-providers/determinist-uuid.generator";

let fleetsRepository: FleetsInMemoryRepository;
let vehiclesRepository: VehiclesInMemoryRepository;
let uuidGenerator: DeterministUuidGenerator;
let handler: RegisterVehicleHandler;

const instantiateServices = (): void => {
  fleetsRepository = new FleetsInMemoryRepository();
  vehiclesRepository = new VehiclesInMemoryRepository();
  uuidGenerator = new DeterministUuidGenerator();
  handler = new RegisterVehicleHandler(
    fleetsRepository,
    vehiclesRepository,
    uuidGenerator
  );
};

beforeEach(async () => {
  instantiateServices();
});

describe("handle", () => {
  it("should register vehicle in the fleet", async () => {
    // Arrange
    fleetsRepository.data = [FleetFixture.withoutVehicle()];

    // Act
    await handler.handle(new RegisterVehicleCommand("f1", "123-456-789"));

    // Assert
    expect(fleetsRepository.saveSpy.vehicles).toStrictEqual([
      VehicleFixture.create({ id: "1", plateNumber: "123-456-789" }),
    ]);
  });

  it("should return the fleet", async () => {
    // Arrange
    fleetsRepository.data = [FleetFixture.withoutVehicle()];

    // Act
    const result = await handler.handle(
      new RegisterVehicleCommand("f1", "123-456-789")
    );

    // Assert
    expect(result).toStrictEqual(
      FleetFixture.withVehicle(
        VehicleFixture.create({ id: "1", plateNumber: "123-456-789" })
      )
    );
  });

  it("should throw when vehicle already registered in the fleet", async () => {
    // Arrange
    fleetsRepository.data = [
      FleetFixture.withVehicle(
        VehicleFixture.create({ plateNumber: "123-456-789" })
      ),
    ];

    try {
      // Act
      await handler.handle(new RegisterVehicleCommand("f1", "123-456-789"));
    } catch (e) {
      // Assert
      expect(e).toStrictEqual(new BusinessError(vehicle_already_registered));
    }
    expect.assertions(1);
  });

  it("should register vehicle when vehicle has been registered in a fleet of an other user", async () => {
    // Arrange
    const alreadyRegisteredVehicle = VehicleFixture.create({
      id: "alreadyRegistered",
      plateNumber: "123-456-789",
    });

    vehiclesRepository.data = [alreadyRegisteredVehicle];
    fleetsRepository.data = [
      FleetFixture.create({ id: "myFleet" }),
      FleetFixture.create({
        id: "anotherFleet",
        vehicles: [alreadyRegisteredVehicle],
      }),
    ];

    // Act
    await handler.handle(new RegisterVehicleCommand("myFleet", "123-456-789"));

    // Assert
    expect(fleetsRepository.saveSpy.vehicles).toStrictEqual([
      alreadyRegisteredVehicle,
    ]);
  });

  it("should throw when fleet does not exist", async () => {
    // Arrange

    try {
      // Act
      await handler.handle(new RegisterVehicleCommand("f1", null));
    } catch (e) {
      // Assert
      expect(e).toStrictEqual(new NotFoundError(fleet_does_not_exist));
    }
    expect.assertions(1);
  });
});
