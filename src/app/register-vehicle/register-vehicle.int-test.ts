import { FleetFixture } from "../../../tests/fixtures/fleet-fixture";
import { FleetsPgRepository } from "../../infra/secondary/postgres/repositories/fleets-pg.repository";
import { AppDataSource } from "../../infra/secondary/postgres/data-source";
import { FleetModel } from "../../infra/secondary/postgres/models/fleet.model";
import { VehicleFixture } from "../../../tests/fixtures/vehicle-fixture";
import { RegisterVehicleHandler } from "./register-vehicle.handler";
import { RegisterVehicleCommand } from "./register-vehicle.command";
import { VehiclesPgRepository } from "../../infra/secondary/postgres/repositories/vehicles-pg.repository";
import { VehicleModel } from "../../infra/secondary/postgres/models/vehicle.model";
import { RealUuidGenerator } from "../../infra/secondary/real-providers/real-uuid.generator";

let fleetsRepository: FleetsPgRepository;
let vehiclesRepository: VehiclesPgRepository;
let uuidGenerator: RealUuidGenerator;
let handler: RegisterVehicleHandler;

const instantiateServices = async (): Promise<void> => {
  const dataSource = await AppDataSource.initialize();
  vehiclesRepository = new VehiclesPgRepository(
    dataSource.getRepository(VehicleModel)
  );
  fleetsRepository = new FleetsPgRepository(
    dataSource.getRepository(FleetModel)
  );
  uuidGenerator = new RealUuidGenerator();
  handler = new RegisterVehicleHandler(
    fleetsRepository,
    vehiclesRepository,
    uuidGenerator
  );
};

beforeAll(async () => {
  await instantiateServices();
});

const clearDatabase = async (): Promise<void> => {
  await vehiclesRepository.deleteAll();
  await fleetsRepository.deleteAll();
};

beforeEach(async () => {
  await clearDatabase();
});

describe("handle", () => {
  it("should register vehicle in the fleet", async () => {
    // Arrange
    await fleetsRepository.save(FleetFixture.withoutVehicle());

    // Act
    await handler.handle(new RegisterVehicleCommand("f1", "123-456-789"));

    // Assert

    const fleets = await fleetsRepository.getAll();
    expect(fleets).toStrictEqual([
      FleetFixture.withVehicle(
        VehicleFixture.create({ id: fleets[0].vehicles[0].id })
      ),
    ]);
  });

  it("should register vehicle when vehicle has been registered in a fleet of an other user", async () => {
    // Arrange
    const alreadyRegisteredVehicle = VehicleFixture.create({
      id: "alreadyRegistered",
      plateNumber: "123-456-789",
    });

    await fleetsRepository.save(FleetFixture.create({ id: "myFleet" }));
    await fleetsRepository.save(
      FleetFixture.create({
        id: "anotherFleet",
        vehicles: [alreadyRegisteredVehicle],
      })
    );

    // Act
    await handler.handle(new RegisterVehicleCommand("myFleet", "123-456-789"));

    // Assert
    const fleets = await fleetsRepository.getAll();
    expect(fleets[0].vehicles).toStrictEqual([alreadyRegisteredVehicle]);
  });

  afterAll(async () => {
    await clearDatabase();
  });
});
