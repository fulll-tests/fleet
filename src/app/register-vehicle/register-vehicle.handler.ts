import { IHandler } from "../_shared/i-handler";
import { IFleetsRepository } from "../../domain/gateways/i-fleets.repository";
import { Fleet } from "../../domain/fleet";
import { Vehicle } from "../../domain/vehicle";
import { IUuidGenerator } from "../../domain/gateways/i-uuid.generator";
import { RegisterVehicleCommand } from "./register-vehicle.command";
import { IVehiclesRepository } from "../../domain/gateways/i-vehicles.repository";
import { NotFoundError } from "../../domain/not-found-error";
import { fleet_does_not_exist } from "../../domain/business-errors";

export class RegisterVehicleHandler implements IHandler {
  constructor(
    readonly fleetsRepository: IFleetsRepository,
    readonly vehiclesRepository: IVehiclesRepository,
    readonly uuidGenerator: IUuidGenerator
  ) {}

  async handle(command: RegisterVehicleCommand): Promise<Fleet> {
    const fleet = await this.getFleet(command);
    const vehicle = await this.getOrCreateVehicle(command.vehiclePlateNumber);

    fleet.register(vehicle);

    await this.fleetsRepository.save(fleet);
    return fleet;
  }

  // PRIVATE METHODS
  private async getFleet(command: RegisterVehicleCommand): Promise<Fleet> {
    const fleet = await this.fleetsRepository.getById(command.fleetId);
    if (!fleet) throw new NotFoundError(fleet_does_not_exist);
    return fleet;
  }

  private async getOrCreateVehicle(
    vehiclePlateNumber: string
  ): Promise<Vehicle> {
    const vehicle = await this.vehiclesRepository.getByPlateNumber(
      vehiclePlateNumber
    );

    if (vehicle) return vehicle;

    return Vehicle.create(this.uuidGenerator, vehiclePlateNumber);
  }
}
