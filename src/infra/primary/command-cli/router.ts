/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { DependencyContainer } from "../../../config/dependency-container";
import { CreateFleetCommand } from "../../../app/create-fleet/create-fleet.command";
import { IEntity } from "../../../app/_shared/i-entity";
import { RegisterVehicleCommand } from "../../../app/register-vehicle/register-vehicle.command";
import { ParkVehicleCommand } from "../../../app/park-vehicle/park-vehicle.command";

export class Router {
  constructor(readonly dependencyContainer: DependencyContainer) {}

  private map = new Map<string, (argv: string[]) => Promise<IEntity>>([
    ["create", (argv) => this.create(argv)],
    ["register-vehicle", (argv) => this.registerVehicle(argv)],
    ["localize-vehicle", (argv) => this.localiseVehicle(argv)],
  ]);

  getHandlerExecute(useCase: string): (argv: string[]) => Promise<IEntity> {
    return this.map.get(useCase);
  }

  // PRIVATE METHODS

  private create(argv: string[]) {
    return this.dependencyContainer.createFleetHandler.handle(
      new CreateFleetCommand(argv[3])
    );
  }

  private registerVehicle(argv: string[]) {
    return this.dependencyContainer.registerVehicleHandler.handle(
      new RegisterVehicleCommand(argv[3], argv[4])
    );
  }

  private localiseVehicle(argv: string[]) {
    return this.dependencyContainer.parkVehicleHandler.handle(
      new ParkVehicleCommand(
        argv[3],
        argv[4],
        Number(argv[5]),
        Number(argv[6]),
        argv[7] ? Number(argv[7]) : null
      )
    );
  }
}
