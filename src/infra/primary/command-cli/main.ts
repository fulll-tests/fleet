import { DependencyContainer } from "../../../config/dependency-container";
import { Router } from "./router";

const handleRequest = async (router: Router): Promise<void> => {
  const useCase = process.argv[2];
  const executeHandler = router.getHandlerExecute(useCase);
  if (!executeHandler) throw new Error("The usecase is not known");

  const result = await executeHandler(process.argv);
  console.log(result.id);
};

const main = async (): Promise<void> => {
  const dependencyContainer = new DependencyContainer();
  await dependencyContainer.init();
  const router = new Router(dependencyContainer);

  try {
    await handleRequest(router);
  } catch (e) {
    console.error(e.message);
  }

  process.exit(0);
};

main();
