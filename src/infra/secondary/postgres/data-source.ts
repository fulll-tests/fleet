import "reflect-metadata";
import { DataSource } from "typeorm";
import { FleetModel } from "./models/fleet.model";
import { VehicleModel } from "./models/vehicle.model";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: process.env.POSTGRES_URL || "localhost",
  port: 5432,
  username: "postgres",
  password: "password",
  database: "fleet",
  entities: [FleetModel, VehicleModel],
  synchronize: true,
  logging: false,
});
