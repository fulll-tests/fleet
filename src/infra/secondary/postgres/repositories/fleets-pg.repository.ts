import { IFleetsRepository } from "../../../../domain/gateways/i-fleets.repository";
import { Fleet } from "../../../../domain/fleet";
import { Repository } from "typeorm";
import { FleetModel } from "../models/fleet.model";
import { plainToInstance } from "class-transformer";
import { Vehicle } from "../../../../domain/vehicle";
import { Location } from "../../../../domain/location";
import { User } from "../../../../domain/user";

export class FleetsPgRepository implements IFleetsRepository {
  constructor(readonly repository: Repository<FleetModel>) {}

  async save(fleet: Fleet): Promise<void> {
    const model = plainToInstance(FleetModel, fleet);
    await this.repository.save(model);
  }

  async getById(id: string): Promise<Fleet> {
    const fleet = await this.repository.findOneBy({ id });
    return this.mapToEntity(fleet);
  }

  async getAll(): Promise<Fleet[]> {
    const fleets = await this.repository.find();
    return fleets.map((fleet) => this.mapToEntity(fleet));
  }

  async deleteAll(): Promise<void> {
    await this.repository.delete({});
  }

  // PRIVATE METHODS
  private mapToEntity(fleet: FleetModel): Fleet {
    return plainToInstance(Fleet, fleet, {
      targetMaps: [
        {
          target: Fleet,
          properties: {
            vehicles: Vehicle,
            user: User,
          },
        },
        {
          target: Vehicle,
          properties: {
            location: Location,
          },
        },
      ],
    });
  }
}
