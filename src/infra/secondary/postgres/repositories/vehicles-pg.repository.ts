import { Repository } from "typeorm";
import { plainToInstance } from "class-transformer";
import { VehicleModel } from "../models/vehicle.model";
import { Vehicle } from "../../../../domain/vehicle";
import { Location } from "../../../../domain/location";
import { IVehiclesRepository } from "../../../../domain/gateways/i-vehicles.repository";

export class VehiclesPgRepository implements IVehiclesRepository {
  constructor(readonly repository: Repository<VehicleModel>) {}

  async save(vehicle: Vehicle): Promise<void> {
    await this.repository.save(plainToInstance(VehicleModel, vehicle));
  }

  async getByPlateNumber(plateNumber: string): Promise<Vehicle> {
    const vehicle = await this.repository.findOne({
      where: { plateNumber: plateNumber },
    });
    return this.mapToEntity(vehicle);
  }

  async getAll(): Promise<Vehicle[]> {
    const vehicles = await this.repository.find();
    return vehicles.map((vehicle) => this.mapToEntity(vehicle));
  }

  async deleteAll(): Promise<void> {
    await this.repository.delete({});
  }

  // PRIVATE METHODS

  private mapToEntity(vehicle: VehicleModel): Vehicle {
    return plainToInstance(Vehicle, vehicle, {
      targetMaps: [
        {
          target: Vehicle,
          properties: {
            location: Location,
          },
        },
      ],
    });
  }
}
