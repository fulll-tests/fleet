import { Entity, PrimaryColumn, OneToMany, Column } from "typeorm";
import { VehicleModel } from "./vehicle.model";

export class UserModel {
  @Column()
  id: string;
}

@Entity()
export class FleetModel {
  @PrimaryColumn()
  readonly id: string;

  @Column(() => UserModel)
  readonly user: UserModel;

  @OneToMany(() => VehicleModel, (vehicle) => vehicle.fleet, {
    cascade: true,
    eager: true,
  })
  readonly vehicles: VehicleModel[];
}
