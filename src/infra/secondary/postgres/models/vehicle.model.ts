import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from "typeorm";
import { FleetModel } from "./fleet.model";

export class LocationModel {
  @Column({ nullable: true })
  readonly lat: number;

  @Column({ nullable: true })
  readonly lng: number;

  @Column({ nullable: true })
  readonly alt: number;
}

@Entity()
export class VehicleModel {
  @PrimaryColumn()
  readonly id: string;

  @Column()
  readonly plateNumber: string;

  @Column(() => LocationModel)
  readonly location: LocationModel;

  @ManyToOne(() => FleetModel, (fleet) => fleet.vehicles)
  @JoinColumn({})
  fleet: FleetModel;
}
