import { IFleetsRepository } from "../../../domain/gateways/i-fleets.repository";
import { Fleet } from "../../../domain/fleet";

export class FleetsInMemoryRepository implements IFleetsRepository {
  data: Fleet[] = [];
  saveSpy: Fleet = null;

  async save(fleet: Fleet): Promise<void> {
    this.saveSpy = fleet;
  }

  async getById(id: string): Promise<Fleet> {
    return this.data.find((fleet) => fleet.id === id);
  }
}
