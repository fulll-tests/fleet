import { Vehicle } from "../../../domain/vehicle";
import { IVehiclesRepository } from "../../../domain/gateways/i-vehicles.repository";

export class VehiclesInMemoryRepository implements IVehiclesRepository {
  data: Vehicle[] = [];
  saveSpy: Vehicle;

  async getByPlateNumber(plateNumber: string): Promise<Vehicle> {
    return this.data.find((vehicle) => vehicle.plateNumber === plateNumber);
  }

  async save(vehicle: Vehicle): Promise<void> {
    this.saveSpy = vehicle;
  }
}
