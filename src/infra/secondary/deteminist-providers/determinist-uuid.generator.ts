import { IUuidGenerator } from "../../../domain/gateways/i-uuid.generator";

export class DeterministUuidGenerator implements IUuidGenerator {
  generate(): string {
    return "1";
  }
}
