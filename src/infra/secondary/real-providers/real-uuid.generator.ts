import { IUuidGenerator } from "../../../domain/gateways/i-uuid.generator";
import { v4 as uuidv4 } from "uuid";

export class RealUuidGenerator implements IUuidGenerator {
  generate(): string {
    return uuidv4();
  }
}
