FROM node:18.16.0
WORKDIR /app
COPY package.json yarn.lock .eslintrc.json ./
RUN yarn install && yarn cache clean --force

COPY tests tests

COPY src src
COPY tsconfig.json ./

CMD ["node"]
