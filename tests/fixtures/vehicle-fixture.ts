import { Vehicle } from "../../src/domain/vehicle";
import { Location } from "../../src/domain/location";

export class VehicleFixture {
  static create({
    id = "v1",
    plateNumber = "123-456-789",
    location = new Location(null, null, null),
  }): Vehicle {
    return new Vehicle(id, plateNumber, location);
  }

  static withLocation(location: Location): Vehicle {
    return VehicleFixture.create({ location });
  }
}
