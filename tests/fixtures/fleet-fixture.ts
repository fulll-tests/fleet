import { Fleet } from "../../src/domain/fleet";
import { Vehicle } from "../../src/domain/vehicle";
import { User } from "../../src/domain/user";

export class FleetFixture {
  static create({ id = "f1", user = new User("u1"), vehicles = [] }): Fleet {
    return new Fleet(id, user, vehicles);
  }

  static withoutVehicle(): Fleet {
    return FleetFixture.create({});
  }

  static withVehicle(vehicle: Vehicle): Fleet {
    return FleetFixture.create({ vehicles: [vehicle] });
  }
}
