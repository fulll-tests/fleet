process.env.TZ = "GMT";

module.exports = {
  moduleFileExtensions: ["js", "ts", "json"],
  rootDir: "../../src",
  testRegex: ".int-test.ts$",
  transform: {
    "^.+\\.(t|j)s$": "ts-jest",
  },
  testEnvironment: "node",
};
